from RungeKutta import *
from Common import *
from math import cos
from ast import literal_eval
import matplotlib.pyplot as plt

def f(t, y):
    return Vector(cos(t+1.1*y[1])+y[0], -y[1]**2 + 2.1*y[0] + 1.1)

def datadiff(data1, data2):
    return [[p1[0], [abs(y1-y2) for y1,y2 in zip(p1[1],p2[1])]] for
            p1,p2 in zip(data1,data2)]

mpm = RungeKutta(stepT=0.1, startY=Vector(0.25, 1), f=f,
        method=MidpointMethod)
rkm = RungeKutta(stepT=0.1, startY=Vector(0.25, 1), f=f,
        method=RungeKuttaMethod)

with open("data/y1.dat") as y1dat, open("data/y2.dat") as y2dat:
    chkplt = [literal_eval(y1dat.read()), literal_eval(y2dat.read())]

mpmplt = mpm.getplotdata(left=0, right=3)
rkmplt = rkm.getplotdata(left=0, right=3)

plt.style.use('grayscale')

plt.grid(True)
plt.plot(*rkmplt[0], '--', zorder=1, label='$y_1$ Runge-Kutta')
plt.plot(*mpmplt[0], '--', zorder=2, label='$y_1$ Middle Point')
plt.plot(*chkplt[0], '-',  zorder=0, label='$y_1$ Mathematica 11.1')
plt.legend()

plt.show(block=True)

plt.grid(True)
plt.plot(*rkmplt[1], '--', zorder=1, label='$y_2$ Runge-Kutta')
plt.plot(*mpmplt[1], '--', zorder=2, label='$y_2$ Middle Point')
plt.plot(*chkplt[1], '-',  zorder=0, label='$y_2$ Mathematica 11.1')
plt.legend()

plt.show(block=True)

mpmerrplt = datadiff(mpmplt, chkplt)
rkmerrplt = datadiff(rkmplt, chkplt)

plt.grid(True)
plt.plot(*rkmerrplt[0], label='$y_1$ Runge-Kutta Error')
plt.plot(*mpmerrplt[0], label='$y_1$ Middle Point Error')
plt.legend()

plt.show(block=True)

plt.grid(True)
plt.plot(*rkmerrplt[1], label='$y_2$ Runge-Kutta Error')
plt.plot(*mpmerrplt[1], label='$y_2$ Middle Point Error')
plt.legend()

plt.show(block=True)
