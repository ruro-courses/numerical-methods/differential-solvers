from RungeKutta import *
from Common import *
from math import sin
from ast import literal_eval
import matplotlib.pyplot as plt

def f(t, y):
    return Vector(-y[0]-t**2)

def datadiff(data1, data2):
    return [[p1[0], [abs(y1-y2) for y1,y2 in zip(p1[1],p2[1])]]
            for p1,p2 in zip(data1,data2)]

mpm = RungeKutta(stepT=0.1, startY=Vector(10), f=f, method=MidpointMethod)
rkm = RungeKutta(stepT=0.1, startY=Vector(10), f=f, method=RungeKuttaMethod)

with open("data/y.dat") as ydat:
    chkplt = [literal_eval(ydat.read())]

mpmplt = mpm.getplotdata(left=0, right=5)
rkmplt = rkm.getplotdata(left=0, right=5)

plt.style.use('grayscale')

plt.grid(True)
plt.plot(*rkmplt[0], '--', zorder=1, label='$y$ Runge-Kutta')
plt.plot(*mpmplt[0], '--', zorder=2, label='$y$ Middle Point')
plt.plot(*chkplt[0], '-',  zorder=0, label='$y$ Mathematica 11.1')
plt.legend()

plt.show(block=True)

mpmerrplt = datadiff(mpmplt, chkplt)
rkmerrplt = datadiff(rkmplt, chkplt)

plt.grid(True)
plt.plot(*rkmerrplt[0], label='$y$ Runge-Kutta Error')
plt.plot(*mpmerrplt[0], label='$y$ Middle Point Error')
plt.legend()

plt.show(block=True)
